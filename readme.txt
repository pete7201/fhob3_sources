Five Haunts of Bill 3
http://pete7201.oyo.ink/freddy
2015 pete7201 and ToyFreddy69
Inspired by ToyFreddy69 and Foxymangle Chica (check them out on YouTube) (I am Peter Taranto on YouTube)
Based on Five Nights at Freddy's (mainly FNaF3 but also uses FNaF1 elements) and Freddy Fazbear's Pizza - 30 Years Later beta as of 6/17/15
yes I decompiled it, and yes FNaF3 and FFCN are NOT decompile-able by Anaconda #fail #sucks4me #alternative

Install/Overwrite the fonts in the Fonts folder before playing, or the game text will look shitty! Also make sure your graphics card supports DirectX 9.0c and Pixel Shaders 2.49 or higher or the game will also look shitty.
Having an old or no GPU equals effects won't be displayed correctly, any newer than 2005 computer will be able to run this game at it's target 50 fps and yes I know it's not 60 with full effects
Scan lines are normal and intended because I used fxOS on most GUI elements
So make sure you don't have a shitty/old computer and the game won't look shitty! ;)

To play FHOB3 extract the zip and run fhob3.exe, it takes a while to load and render textures.
As usual there is a video that plays when you start the game, however in FHOB 3, instead of the video playing the game itself renders textures.
A new video player window will be opened and you can close it as soon as the textures finish rendering or the video (about 2 minutes) ends. Or you can alt f4 and look at the nice load screen I made. ;)
For me it takes about 30 seconds for the textures to render, but that's with an i7 laptop, a GTX 850m, and 16 gb of RAM #overkill #fuckit #fast #insane
If it fails to launch with the error that LocalIP can't find dependencies install the Visual C++ Redistributable, and if Onu can't find something make sure you have OnuEdit.dll
If the game takes a long time to load run it with commandline "-fastboot" but this will make multiplayer error out so its disabled when you're using fastboot
To play FHOB3 multiplayer first beat the game then select "Multiplayer" on the title. Up to two players can play FHOB3 multiplayer.
Player 1 is the animatronic and hosts the server. Name yourself and wait for Player 2 (the guard) to connect to either the IP or hostname shown.
Enter the IP into the box and click Connect, or click Autodetect to search for a FHOB3 server (Local only! Will NOT search for Internet servers)
The sniffer program included will ping every IP on your local network, and try to connect to the IPs that respond. If it detects a FHOB3 server it will fill the box for the IP of the FHOB3 server.
If you have multiple servers running at the same time, the first one detected will be shown.
I did not make this program part of the FHOB3 exe because the sniffer crashes if run too long, or if it can't connect to a network because #clickteam #ISIS #dealwithit #weed

I added in the FHOB 3 multiplayer menu a custom profile picture icon. In older FHOB games you could name yourself, but in FHOB 3, you can also set a custom or preloaded profile picture.
You can import one by clicking on the import box, or off to the side you will find some fnaf icons. Click on the "fnaficons" to cycle through the built in 16 fnaficons. (They're in the fnaficons folder /r/technicalFNaF!)
If you want to get FHOB3 you can if you connect to someone who is playing FHOB3's IP address while the fnaficon server is running and type in <IP address>/fhob3.exe (for me it's 192.168.1.128/fhob3.exe) This was part of the server and its cool and dank so I didn't remove it.

For me, the IP is either 10.0.1.128 or 192.168.1.2 but it can be any set of four numbers OTHER THAN 0.0.0.0, 127.0.0.1, or 255.255.255.255
The port is always port 1982 and 1983 but the game already knows. You can port forward TCP and UDP port 1982 and 1983 on your router to play over the internet.
Player 1's hostname is the next set of text. You can put this in instead of Player 1's IP. For me, it's Peter-PCM because that's what the computer name of my laptop is.
When you connect, Player 1 will recieve Player 2's name and vice versa. If you don't put in a name, your name becomes "unnamed" and some random numbers.
If Player 2 (the guard) makes it to 6am then Player 1 (the animatronic) will get wasted and allahu akbar. If Player 1 kills Player 2 then mission passed for Player 1!
Time is synchronized between both players. The guard's time is used.
Good Luck!!! Yes this is complicated, but it doesn't crash as much as the shitty FHOB1 networking. Protip to other fnaf fangame makers: Don't use the built in fusion 2.5 networking, use something like Lacewing ;)

When you play multiplayer FHOB3 and press Tab while playing, you can chat with the other player. It takes a sec to load depending on cpu speed so the game may freeze when you press Tab.
The character limit is 72 characters so the game doesn't crash or anything. (Just send 2+ messages if that's how long your new rant is)
Press Tab again to exit the chat and return to the game.

Introduced in FHOB3 is the new music player menu. FHOB1 and 2 have menu music but starting in FHOB3 you will have a small interface for controlling it.
It's powered by the Onu audio system so you get a small visualizer in the bottom right corner.
You have four control buttons: Play, Pause, Previous, and Next. Putting the mouse near the Music tab will extend it and reveal the buttons.
The music is the same as always, some FNaF music from YouTube, my top 20 list! So if you hear something you made, congrats! I like it ;)
The background ambiance will be muted when the FNaF music is playing because if both play it sounds shitty.

Have fun and remember to check http://pete7201.oyo.ink/freddy and Scottgames!
also if you want to play some fnaf 1 but you don't have a computer, go on http://pete7201.oyo.ink/htmlfnaf (this is a fully working port of fnaf 1 for the computer running on html5) (try using fullscreen with this)