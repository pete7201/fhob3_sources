float     fBlend;
float     xBlend;
// Pixel shader input structure
struct PS_INPUT
{
    float4 Position   : POSITION;
    float2 Texture    : TEXCOORD0;
};

// Pixel shader output structure
struct PS_OUTPUT
{
    float4 Color   : COLOR0;
};

// Global variables
sampler2D Tex0;



PS_OUTPUT ps_xmain( in PS_INPUT In )
{
    // Output pixel
    PS_OUTPUT Out;

    Out.Color = tex2D(Tex0, In.Texture);
	float4 f4 = Out.Color;
	float f = f4.b + f4.g + (f4.r * xBlend) ;
	float g = f4.b + f4.g + (f4.r * xBlend) ;
	Out.Color.r =f;
Out.Color.g = f* fBlend;
Out.Color.b = g* fBlend;
    return Out;
}
// Effect technique
technique tech_main
{

    pass P1
    {
        // shaders
        VertexShader = NULL;
        PixelShader  = compile ps_1_1 ps_xmain();
    }  
}